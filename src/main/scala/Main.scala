import scala.io.Source
import scala.math.Ordering.Implicits.seqOrdering

object Main {
  def main(args: Array[String]): Unit = {
    val FileWithMarks = Source.fromFile("marks.txt")
    val MatchingPattern = "(\\w+):(\\d+)".r

    val MapStudentMark = try {
      FileWithMarks.getLines().map {
        case MatchingPattern(student, mark) => (student, mark.toInt)
      }.toSeq.groupMapReduce(_._1) {
        case (_, mark) => Seq(mark)
      }(_ ++ _)
    } finally FileWithMarks.close()

    println("Число уникальных студентов:")
    println(MapStudentMark.keys.size)

    println("==================Task 1==================")
    MapStudentMark.foreach( mapValue => println(mapValue._1 +" : "+ mapValue._2.sum))
    println("==================Task 2==================")
    MapStudentMark.map{
      case (name, mark) => (name, mark.sum)
    }.toSeq.sortBy(_._2).reverse.foreach(item => println(item._1 + " : " + item._2))
    println("==================Task 3==================")
    MapStudentMark.foreach( mapValue => println(mapValue._1 +" : "+ 1.0 * mapValue._2.sorted.sum / mapValue._2.size))
    println("==================Task 4==================")
    val flattened_student_scores = MapStudentMark.values.flatten.toSeq.toIndexedSeq
    println(flattened_student_scores.sum)
    println("==================Task 5==================")
    println(flattened_student_scores.sorted.apply(flattened_student_scores.size / 2))
    println("==================Task 6==================")
    println(flattened_student_scores.groupMapReduce(identity)(_ => 1)(_ + _).maxBy(_._2)._1)
  }
}